/* 
 * File:   main.cpp
 * Author: Miguel Angel Marquez, Antonio Leyva
 *
 * Created on 23 de septiembre de 2018, 03:59 PM
*/
#include <stdio.h>	// printf y scanf, NULL
#include <stdlib.h>	// rand, srand, malloc, free
#include <string.h>     // memcpy
#include <time.h>
#include "mplib.h"

int main(){    
    /*valores para operacion; 
     * 1 para suma        
     * 2 para resta
     * 3 para multiplicacion escuela
     * 4 para multiplicacion karatsuba
     * 5 para barret
     * 6 para montgomery
     * 7 para euclides
    */    
    int operation = 3;   
    
    //numero de operaciones que se van a hacer.
    int iterations = 10; 
    
    /*variable para indicar si se desea imprimir el resultado;
     * 1 para imprimir
     * 0 para no imprimir resultados
    */    
    int if_print = 1;
    
    //establecer la semilla para generar los valores aleatorios
    srand(time(NULL));
        
    clock_t start, end;
    double cpu_time_used;
    start = clock();
	
    switch(operation){
        case 1: Add_iterations(iterations, if_print, WSIZE);
            break;
        case 2: Sub_iterations(iterations, if_print, WSIZE);
            break;
        case 3: Mul_iterations(iterations, if_print, WSIZE);
            break;
        case 4: Karatsuba_iterations(iterations, if_print, WSIZE);
            break;
        case 5: Barret_iterations(iterations, if_print, WSIZE);
            break;
        case 6: Montgomery_iterations(iterations, if_print, WSIZE);
            break;
		case 7: Euclides_iterations(iterations, if_print, WSIZE);
            break;
   }
    
    printf("\n\n||**************************************||");
    printf("\n||\t\tWORKSPACE\t\t||");
    printf("\n||\t\tBITS: %d\t\t||", BITS);
    printf("\n||\t\tWORD SIZE: %d\t\t||", WSIZE);
    printf("\n||**************************************||");
    
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    
    printf("\n\n||******************************||");
    printf("\n||\tCPU ticks: %f\t||", cpu_time_used);
    printf("\n||******************************||");
    
    printf("\n");
    return 0;
}

