#include <stdio.h>		// printf y scanf, NULL
#include "mplib.h"

void Add_iterations(int iterations, int if_print, int wsize){
    DATATYPE *num1, *num2, *add_res;
    
    //inicializar variables
    MP_init_arr(&num1, wsize);
    MP_init_arr(&num2, wsize);
    MP_init_arr_with_zeros(&add_res, wsize+1);
    
    if(if_print == 1) printf("\n\n||**************  ADD  **************||\n");
    for(int i = 0; i < iterations; i++){
        MP_fill_arr_with_rand(num1, wsize);
        MP_fill_arr_with_rand(num2, wsize);
        MP_add(num1, num2, add_res, wsize);
        if(if_print == 1) Magma_print(num1, num2, add_res, '+', wsize, wsize+1);
    }    
}               

void Sub_iterations(int iterations, int if_print, int wsize){
    DATATYPE *num1, *num2, *sub_res;
    
    //inicializar variables
    MP_init_arr(&num1, wsize);
    MP_init_arr(&num2, wsize);    
    MP_init_arr_with_zeros(&sub_res, wsize);    
    
    if(if_print == 1) printf("\n\n||**************  SUB  **************||\n");
    for(int i = 0; i < iterations; i++){
        MP_fill_arr_with_rand(num1, wsize);
        MP_fill_arr_with_rand(num2, wsize);
        MP_sub(num1, num2, sub_res, wsize);
        if(if_print == 1) Magma_print(num1, num2, sub_res, '-', wsize, wsize);
    }    
}

void Mul_iterations(int iterations, int if_print, int wsize){
    DATATYPE *num1, *num2, *mul_res;
    
    //inicializar variables
    MP_init_arr(&num1, wsize);
    MP_init_arr(&num2, wsize);
    MP_init_arr_with_zeros(&mul_res, wsize*2);
    
    if(if_print == 1) printf("\n\n||**************  MUL SCHOOL  **************||\n");
    for(int i = 0; i < iterations; i++){
        MP_fill_arr_with_rand(num1, wsize);
        MP_fill_arr_with_rand(num2, wsize);
        MP_fill_arr_with_zeros(mul_res, wsize*2);
        MP_mul(num1, num2, mul_res, wsize);
        if(if_print == 1) Magma_print(num1, num2, mul_res, '*', wsize, wsize*2);
    }    
}               

void Karatsuba_iterations(int iterations, int if_print, int wsize){
    DATATYPE *num1, *num2, *mul_karat_res;
    
    //inicializar variables
    MP_init_arr(&num1, wsize);
    MP_init_arr(&num2, wsize);
    MP_init_arr_with_zeros(&mul_karat_res, wsize*2);
        
    if(if_print == 1) printf("\n\n||**************  KARATSUBA  **************||\n");
    for(int i = 0; i < iterations; i++){
        MP_fill_arr_with_rand(num1, wsize);
        MP_fill_arr_with_rand(num2, wsize);
		MP_fill_arr_with_zeros(mul_karat_res, wsize*2);
        MP_karatsuba(num1, num2, mul_karat_res, wsize);
        if(if_print == 1) Magma_print(num1, num2, mul_karat_res, '*', wsize, wsize*2);
    }    
}

void Barret_iterations(int iterations, int if_print, int wsize){
    DATATYPE *num1, *num2, *res_bar;    //variables de barret
    DATATYPE *prime, *mu, *pinv;        //cosntantes prime, pinv, mu
    
    //inicializar variables
    MP_init_arr(&num1, wsize);
    MP_init_arr(&num2, wsize);
    MP_init_arr_with_zeros(&res_bar, wsize);
    //inicializar constantes
    MP_init_arr(&prime, wsize);
    MP_init_arr(&pinv, wsize);    
    MP_init_arr(&mu, wsize+1);
    MP_get_p_mu_pinv(prime, mu, pinv);
    
    if(if_print == 1) printf("\n\n||**************  BARRET  **************||\n");
    for(int i = 0; i < iterations; i++){
        MP_fill_arr_with_rand(num1, wsize);
        MP_fill_arr_with_rand(num2, wsize);        
        MP_barret_red(num1, num2, prime, mu, res_bar, wsize);
        
        //test res_mon eq (num1_bar*num1_bar) mod p;
        if(if_print == 1) Magma_print_mod(num1, num2, prime, res_bar, wsize);   
    }    
}

void Montgomery_iterations(int iterations, int if_print, int wsize){
    
    DATATYPE *num1_bar, *num2_bar, *res_num1_bar, *res_num2_bar;    //variables de barret
    DATATYPE *res_temp, *res_mon, *arr_one;                         //variables de montgomery
    DATATYPE *prime, *mu, *pinv;                  //cosntantes prime, pinv, mu
    
    //inicializar variables Barret
    MP_init_arr_with_zeros(&num1_bar, wsize);    
    MP_init_arr_with_zeros(&num2_bar, wsize);        
    MP_init_arr_with_zeros(&res_num1_bar, wsize+1);    
    MP_init_arr_with_zeros(&res_num2_bar, wsize+1);                
    //inicializar variables Montgomery    
    MP_init_arr_with_zeros(&res_temp, wsize);
    MP_init_arr_with_zeros(&res_mon, wsize);    
    MP_init_arr_with_zeros(&arr_one, wsize);
    arr_one[0]=1;    
    //inicializar constantes
    MP_init_arr(&prime, wsize);
    MP_init_arr(&pinv, wsize);    
    MP_init_arr(&mu, wsize+1);    
    MP_get_p_mu_pinv(prime, mu, pinv);
            
    //generacion del arreglo "most_significant_one"        
        
    if(if_print == 1) printf("\n\n||**************  MONTGOMERY  **************||\n");
    for(int i = 0; i < iterations; i++){
        
        MP_fill_arr_with_boundary_rand(num1_bar, prime, wsize);        
        MP_fill_arr_with_boundary_rand(num2_bar, prime, wsize);             
        
        // converting to Montgomery domain, x := (a*R) mod p;         
        MP_barret_red(num1_bar, arr_one, prime, mu, res_num1_bar, wsize);
        MP_barret_red(num2_bar, arr_one, prime, mu, res_num2_bar, wsize);        
                
        // operaciones de montgomery
        MP_montgomery(res_num1_bar, res_num2_bar, prime, pinv, res_temp, wsize);    
        MP_montgomery(res_temp, arr_one, prime, pinv, res_mon, wsize);
        //printf("\n\n\nres_mon: "); MP_print_arr_LtoR(res_mon, wsize);

		//test res_mon eq (num1_bar*num1_bar) mod p;
        if(if_print == 1) Magma_print_mod(num1_bar, num2_bar, prime, res_mon, wsize);			
                
    }    
}

/*
bits := 64;
palabra := 4;
p := NextPrime(Random((2^bits)^palabra));
Z :=  GF(p);   //campo finito de cardinalidad p
x := Random(p);
y :=  Z  !  x;
w := y^(-1);
1 eq w*y;

W := IntegerRing() ! w;
Y := IntegerRing() ! y;
printf "\nx: %h", x;
printf "\nPrimo: %h", p;
printf "\nW: %h", W;
printf "\nY: %h", Y;
*/
void Euclides_iterations(int iterations, int if_print, int wsize){
    DATATYPE *num1, *prime, *inver, *null1, *null2;
    wsize = 1;
    //inicializar variables
    MP_init_arr(&num1, wsize);
    MP_init_arr(&prime, wsize);
    MP_init_arr(&inver, wsize);
    MP_init_arr(&null1, wsize+1);
    MP_init_arr(&null2, wsize);
    MP_get_p_mu_pinv(prime, null1, null2);
        
    if(if_print == 1) printf("\n\n||**************  EUCLIDES  **************||\n");
    for(int i = 0; i < iterations; i++){
        MP_fill_arr_with_rand(num1, wsize);        
        //MP_convert_to_array(num1, "6B0A238788FCDB7B", wsize);
        //MP_convert_to_array(prime, "9102D85C95A860F9", wsize);        
        
        MP_inverso_AEE(num1, prime, inver, wsize);        
        if(if_print == 1) MP_print_arr_LtoR(num1, wsize); MP_print_arr_LtoR(inver, wsize);
    }
}

void Magma_print(DATATYPE *num1, DATATYPE *num2, DATATYPE *res, char operation, int wsize_operators, int wsize_res){
    printf("0x"); MP_print_arr_LtoR(num1, wsize_operators); 
    printf(" %c ", operation);
    printf("0x"); MP_print_arr_LtoR(num2, wsize_operators);
    printf(" eq ");
    printf("0x"); MP_print_arr_LtoR(res, wsize_res);
    printf(";\n");
}

void Magma_print_mod(DATATYPE *num1, DATATYPE *num2, DATATYPE *prime, DATATYPE *res, int wsize){
    printf("0x"); MP_print_arr_LtoR(res, wsize); printf(" eq ");
    printf("(0x"); MP_print_arr_LtoR(num1, wsize); printf(" * ");
    printf("0x"); MP_print_arr_LtoR(num2, wsize); printf(") mod ");
    printf("0x"); MP_print_arr_LtoR(prime, wsize); printf(";");
    printf("\n");
}