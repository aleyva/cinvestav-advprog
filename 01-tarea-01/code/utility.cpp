
#include <stdio.h> // printf y scanf, NULL
#include <stdlib.h> // rand, srand, malloc, free
#include <string.h>     // memcpy
#include <inttypes.h> // __uint128_t
#include "mplib.h"


DATATYPE Rand_gen(){
    DATATYPE x;
    x = rand(); //hasta esta linea, es suficiente si DATATYPE es de 16 bits
    x |= rand()<<1;		//truco para hacer grandes los valores en x
    if (sizeof(DATATYPE) >= 8) {
        x ^= (DATATYPE)rand() << 32;
        x |= (DATATYPE)rand() << 33; //truco para hacer grandes los valores en x
    }

    return x;
}

void MP_init_arr(DATATYPE** arr, int words){
    *arr = (DATATYPE*)malloc(sizeof(DATATYPE) * words);
}

void MP_init_arr_with_rand(DATATYPE** arr, int words){ 
    *arr = (DATATYPE*)malloc(sizeof(DATATYPE) * words);
    for (int i = 0; i < words; i++) {
        (*arr)[i] = Rand_gen();
    }
}

void MP_init_arr_with_zeros(DATATYPE** arr, int words){ 
    *arr = (DATATYPE*)malloc(sizeof(DATATYPE) * words);
    for (int i = 0; i < words; i++) { 
        (*arr)[i] = 0;
    }
}

void MP_fill_arr_with_rand(DATATYPE* arr, int words){ 
    for (int i = 0; i < words; i++) { 
        arr[i] = Rand_gen();
    }
}

//esta funcion genera un numero random con frontera superior,
//sin embargo esta frontera solo la considera para la palabra mas significativa
void MP_fill_arr_with_boundary_rand(DATATYPE* arr, DATATYPE* boundary, int words){    
    for (int i = 0; i < words; i++) {
        arr[i] = Rand_gen();
    }
    arr[words-1] = arr[words-1] % boundary[words-1];
}

void MP_fill_arr_with_zeros(DATATYPE* arr, int words){ 
    for (int i = 0; i < words; i++) { 
        arr[i] = 0;
    }
}

void MP_fill_char_arr_with_zeros(char* arr, int words){ 
    for (int i = 0; i < words; i++) { 
        arr[i] = 0;
    }
}

//funcion que convierte una cadena de caracteres a un DATATYPE arr[wsize]
//wsize debe ser mayor a 0
void MP_convert_to_array(DATATYPE *arr, char const *hex_arr, int wsize){	    
    int sizeof_int = sizeof(int);
    int sizeof_word_div_int = (BITS/16)-1;    
    int times_copy = 0;
    int word = 0;
    char hex_temp[sizeof_int];
    char *hex_end, *hex_start;
    DATATYPE hex_word = 0;
    
    //entontrar length de hex_arr
    hex_end = (char *)hex_arr;        
    while(hex_end[0] != '\0')
        hex_end++;    
    
    hex_start = hex_end - sizeof_int;
    while(hex_start > hex_arr){
        //extrae el hexadecimal de 16 bits a hex_word
        //esto se hace, ya que la funcion strtol, no puede convertir algo mayor a un int (16 bits) a hexadecimal
        memcpy(hex_temp, hex_start, sizeof_int);				
        hex_word ^= (DATATYPE)strtol(hex_temp, NULL, 16) << (16*times_copy);
        
        //recorre las posiciones para extraer los hexadecimales
        hex_end = hex_start;
        hex_start = hex_end - sizeof_int;
                
        //verifica cuantas veces ha extraido los hexadecimales, si ha juntado una palabra, entonces la guarda y cambia de palabra.
        if(times_copy >= sizeof_word_div_int){            
            arr[word] = hex_word;            
            hex_word = 0;
            times_copy = 0;
            word++;
            //sale de la función si se ha alcanzado wsize (aun si quedaran caracteres por copiar)
            if(word == wsize)
                return;
        }else  
            times_copy++;
    }
    //convierte a hexadecimal los ultimos caracteres y los coloca en arr[word]
    //corrige si la posicion de hex_start es menor a hex_arr
    if(hex_start < hex_arr)
        hex_start = (char *)hex_arr;
    
    MP_fill_char_arr_with_zeros(hex_temp, sizeof_int);
    memcpy(hex_temp, hex_start, (hex_end - hex_start) );				
    hex_word ^= (DATATYPE)strtol(hex_temp, NULL, 16) << (16*times_copy);
    arr[word] = hex_word;    
}

void MP_shift_array_wsize_right(DATATYPE* start, int size_start, int shift, DATATYPE* end, int size_end){
    int i, j;
    for (i = 0, j = 0; i < size_end; i++) {
        if (i >= shift && j < size_start) { 
            end[i] = start[j];
            j++;
        }
        else {
            end[i] = 0;
        }
    }
}

//esto es una division entre 2
void MP_bit_shift_left(DATATYPE* arr, int wsize)
{   
    DATATYPE temp = 0;
    int i;
    for (i = 0; i < wsize-1; i++) {
        arr[i] = arr[i] >> 1;
        arr[i] ^= (arr[i + 1] & 0b1) << (BITS - 1);        
    }
    arr[i] = arr[i] >> 1;
}

//devuelve 1 si es par, devuelve 0 si es non
int MP_is_even(DATATYPE* arr){
    if ((arr[0] & 1) == 0)
        return 1;
    else
        return 0;
}

//devuelve 1 si es non, devuelve 0 si es par
int MP_is_odd(DATATYPE* arr){
    if ((arr[0] & 1) == 1)
        return 1;
    else
        return 0;
}

//devuelve 1 si en todo el arreglo hay ceross, devuelve 0 si no
int MP_is_zero(DATATYPE* arr, int words){
    for (int i = 0; i < words; i++) { //para i desde word size-1 hasta 0, imprimir la entrada i del arreglo en hexadec. con 4 dig.
        if (arr[i] != 0)
            return 0;
    }
    return 1;
}

//devuelve 1 si arr es 1, devuelve 0 si no
//ejemplo   0x00000000000001   devuelve 1
int MP_is_one(DATATYPE* arr, int words){
    if(arr[0] == 1){
        for (int i = 1; i < words; i++) { 
            if (arr[i] != 0)
                return 0;
        }
        return 1;
    }
    return 0;
}

//devuelve 1 si num1 es mayor que num2
int MP_is_greater_or_equal_than(DATATYPE* num1, DATATYPE* num2, int wsize){
    for (int i = wsize - 1; i >= 0; i--) {
        if (num1[i] > num2[i])
            return 1;
        else if (num1[i] < num2[i])
            return 0;
    }
    return 1;
}


void MP_print_2array_inline(DATATYPE* num1, DATATYPE* num2, int words){
    printf(" ");
    MP_print_arr_LtoR(num1, words);
    printf(" * ");
    MP_print_arr_LtoR(num2, words);
}

/*
function MP_print_arr_LtoR_aligned_left
Input:
	DATATYPE num[WORDS] : arreglo a imprimir
	int words : tamaï¿½o del arreglo
	int allwsize : tamaï¿½o de impresion deseado
Output:
	 void
Resume: imprime un arreglo de tamaï¿½o Wsize mostrando la palabra mas significativa del lado izquiedo.
	La impresion agrega "allwsize-words" de palabras de 0's del lado izquierdo.
Example:
	int words = 2, int allwsize = 4
	0000-0000-f3e3-1b4a
	int words = 1, int allwsize = 5
	0000-0000-0000-0000-1b4a
*/
void MP_print_arr_LtoR_aligned_left(DATATYPE* num, int words, int allwsize){
    printf("\n");
    DATATYPE zeros = 0;
    for (int i = allwsize - 1; i >= 0; i--) {
        //si i es menor a WORDS, imprime la palabra del arreglo num[i], de lo contrario imprime palabras de 0's
        if (i < words) printf(PRINT, num[i]);        
        else printf(PRINT, zeros);        
        if(i > 0) printf("-"); //si i es mayor que 0 imprimir un guion
    }
}

void MP_print_arr_LtoR(DATATYPE* num, int words){
    //printf("\n");
    for (int i = words - 1; i >= 0; i--) {         
        printf(PRINT, num[i]);
        //if (i > 0) printf("-"); //si i es mayor que 0 imprimir un guion
    }
}

void MP_print_arr_RtoL(DATATYPE* num, int words){
    printf("\n");
    for (int i = 0; i < words; i++) {
        printf(PRINT, num[i]);
        if (i < words - 1) printf("-");  //si i es mayor que 0 imprimir un guion
    }
}
