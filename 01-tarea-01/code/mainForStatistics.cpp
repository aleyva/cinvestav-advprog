/* 
 * File:   main.cpp
 * Author: Miguel Angel Marquez, Antonio Leyva
 *
 * Created on 23 de septiembre de 2018, 03:59 PM
*/
#include <stdio.h>	// printf y scanf, NULL
#include <stdlib.h>	// rand, srand, malloc, free
#include <string.h>     // memcpy
#include <time.h>
#include "mplib.h"

int main(){    
    int wsize = 35; //las constantes están hasta 12
    
    //numero de operaciones que se van a hacer.
    int iterations = 10; 
    
    /*variable para indicar si se desea imprimir el resultado;
     * 1 para imprimir
     * 0 para no imprimir resultados
    */    
    int if_print = 1;
    
    //definicion del arreglo de resultados
    int total_ops = 6;
    int total_times_try = 6;    
    double arr_results[wsize][total_ops][total_times_try];
    
    //establecer la semilla para generar los valores aleatorios
    srand(time(NULL));
        
    clock_t start, end;
    double cpu_time_used;
    
    for(int wsize_iter = 1; wsize_iter <= wsize; wsize_iter++){
        for(int num_operation = 4; num_operation < total_ops; num_operation++){
            for (int times_try = 0; times_try < total_times_try; times_try++){

                start = clock();

                switch(num_operation){
                    case 0: if(if_print == 1) printf("\n||Prueba No. %d **************||", times_try);
                            Add_iterations(iterations, if_print, wsize_iter);
                        break;
                    case 1: if(if_print == 1) printf("\n||Prueba No. %d **************||", times_try);
                            Sub_iterations(iterations, if_print, wsize_iter);
                        break;
                    case 2: if(if_print == 1) printf("\n||Prueba No. %d **************||", times_try);
                            Mul_iterations(iterations, if_print, wsize_iter);
                        break;
                    case 3: if(if_print == 1) printf("\n||Prueba No. %d **************||", times_try);
                            Karatsuba_iterations(iterations, if_print, wsize_iter);
                        break;
                    case 4: if(if_print == 1) printf("\n||Prueba No. %d **************||", times_try);
                            Barret_iterations(iterations, if_print, wsize_iter);
                        break;
                    case 5: if(if_print == 1) printf("\n||Prueba No. %d **************||", times_try);
                            Montgomery_iterations(iterations, if_print, wsize_iter);
                        break;
                    case 6: if(if_print == 1) printf("\n||Prueba No. %d **************||", times_try);
                            //Euclides_iterations(iterations, if_print, wsize_iter);
                        break;
                }

                end = clock();
                cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

                arr_results[wsize_iter-1][num_operation][times_try] = cpu_time_used;
            }
        }
    }
    
    //Print_results(arr_results[0][0], iterations, total_ops, total_times_try, wsize);
	printf("\n\n||**************************************||");
    printf("\n||\t\tWORKSPACE\t\t||");
    printf("\n||\t\tBITS: %d\t\t||", BITS);
    printf("\n||\t\tWORD SIZE: %d\t\t||", wsize);
    printf("\n||**************************************||");
    
    printf("\n\n||**************************************||");
    printf("\n||\tIteraciones op realizadas: %d\t||", iterations);
    printf("\n||**************************************||");
    
    double average = 0;
    for(int wsize_iter = 1; wsize_iter <= wsize; wsize_iter++){
        printf("\n\n||**************************************||");
        printf("\n||\t\twsize: %d\t\t||", wsize_iter);        
        printf("\n||**************************************||\n");
        for(int num_operation = 0; num_operation < total_ops; num_operation++){
            switch(num_operation){
                case 0: printf("\n\t|| SUMA      ");
                    break;
                case 1: printf("\n\t|| RESTA     ");
                    break;
                case 2: printf("\n\t|| MULT ESC  ");
                    break;
                case 3: printf("\n\t|| KARATSUBA ");
                    break;
                case 4: printf("\n\t|| BARRET    ");
                    break;
                case 5: printf("\n\t|| MONTGOMERY");
                    break;
                case 6: printf("\n\t|| EUCLIDES  ");
                    break;
            }
        
			/*
			printf("\n\t");
            for (int times_try = 0; times_try < total_times_try; times_try++){
                printf("\tPrueba No. %d,\t",times_try);            
            }
            printf("\n\t");
            for (int times_try = 0; times_try < total_times_try; times_try++){            
                printf("\t%f,\t", arr_results[wsize_iter-1][num_operation][times_try]);            
            }*/

			//printf("\n\t");
			average = 0;
            for (int times_try = 1; times_try < total_times_try; times_try++){            
            	average += arr_results[wsize_iter-1][num_operation][times_try];       
            }
			printf("\tpromedio: %f,\t", average/(total_times_try-1));
			
        }
    }
	
            
    printf("\n");
    return 0;
}

//compiarl mainForStatistics
//g++ mainForStatistics.cpp op_iter.cpp arith.cpp utility.cpp constant.cpp -o outputStatistics