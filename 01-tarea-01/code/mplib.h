
#include <inttypes.h>   // __uint128_t

#ifndef WORK_SPACE
	#define BITS 64	//con cuantos bits se va a trabajar, opciones 16, 32 y 64
	#define WSIZE 8		//palabras con las que se va a trabajar
    #define WORK_SPACE
#endif /* WORK_SPACE */


#ifndef MPLIB_H

    #if BITS == 16
        //#define DATATYPE uint16_t  			
        //#define DOUBLEDATATYPE uint32_t
	#define DATATYPE unsigned int
	#define DOUBLEDATATYPE long unsigned int
        #define PRINT "%.4x"  				// "%04x" imprime hexadecimal con 4 digitos exactamente (para 16 bits)
        #define FMASKDATATYPE 0xFFFF		//0xffff para 16bits
    #elif BITS == 32
        //#define DATATYPE uint32_t  			
        //#define DOUBLEDATATYPE uint64_t 
	#define DATATYPE long unsigned int
	#define DOUBLEDATATYPE long long unsigned int
        #define PRINT "%.8lx"  				// "%08x" imprime hexadecimal con 8 digitos exactamente (para 32 bits)
        #define FMASKDATATYPE 0xFFFFFFFF	//0xFFFFFFFF para 32bits
    #else
        //#define DATATYPE uint64_t  			
	#define DATATYPE long long unsigned int
        #define DOUBLEDATATYPE __uint128_t  
        #define PRINT "%.16llx"  					// "%08x" imprime hexadecimal con 16 digitos exactamente (para 64 bits)
        #define FMASKDATATYPE 0xFFFFFFFFFFFFFFFF	//0xFFFFFFFF para 32bits
    #endif

    //**********************************//
    //      prototipos de funciones     //
    //**********************************//

    //funciones del archivo constant        
    void MP_get_p_mu_pinv(DATATYPE *, DATATYPE *, DATATYPE *);

    //funciones de utilería 
    DATATYPE Rand_gen();
    void MP_init_arr(DATATYPE **, int);
    void MP_init_arr_with_rand(DATATYPE **, int);    
    void MP_init_arr_with_zeros(DATATYPE **, int);
    void MP_fill_arr_with_rand(DATATYPE*, int);
    void MP_fill_arr_with_zeros(DATATYPE*, int);
    void MP_fill_arr_with_boundary_rand(DATATYPE *, DATATYPE *, int);
    void MP_fill_char_arr_with_zeros(char*, int);    
    void MP_convert_to_array(DATATYPE *, char const *, int);
    void MP_convert_to_array(DATATYPE *, char const *, int);
    void MP_shift_array_wsize_right(DATATYPE *, int, int, DATATYPE *, int);
    void MP_bit_shift_left(DATATYPE *, int);
    int MP_is_even(DATATYPE *);
    int MP_is_odd(DATATYPE *);
    int MP_is_zero(DATATYPE *, int);
    int MP_is_one(DATATYPE*, int);
    int MP_is_greater_or_equal_than(DATATYPE *, DATATYPE *, int);
    void MP_print_2array_inline(DATATYPE *, DATATYPE *, int);       //funcion para karatsuba
    void MP_print_arr_LtoR_aligned_left(DATATYPE *, int, int);      //impresion mostrando la palabra mas significativa del lado izquiedo ?????
    void MP_print_arr_LtoR(DATATYPE *, int);                        //impresion mostrando la palabra mas significativa del lado izquiedo
    void MP_print_arr_RtoL(DATATYPE *, int);                        //impresion mostrando la palabra mas significativa del lado drecho
    
    //funciones de aritmetica multiprecision
    void MP_add(const DATATYPE *, const DATATYPE *, DATATYPE *, int);           // Suma multiprecision, suma hasta int-1 y deja carry en int
    void MP_add_nocarry(const DATATYPE *, const DATATYPE *, DATATYPE *, int);   // Suma multiprecision, suma hasta int
    void MP_sub(const DATATYPE *, const DATATYPE *, DATATYPE *, int);
    void MP_sub_inv_add(DATATYPE *, DATATYPE *, DATATYPE *, int);
    void MP_mul(DATATYPE *, DATATYPE *, DATATYPE *, int);
    void MP_karatsuba(DATATYPE *, DATATYPE *, DATATYPE *, int);
    void MP_barret_red(DATATYPE *, DATATYPE *, DATATYPE *, DATATYPE *, DATATYPE *, int);
    void MP_montgomery(DATATYPE *, DATATYPE *, DATATYPE *, DATATYPE *, DATATYPE *, int);
    void MP_inverso_AEE(DATATYPE *, DATATYPE *, DATATYPE *, int);
    
	//funciones del archivo op_iter	
	void Add_iterations(int, int, int);
	void Sub_iterations(int, int, int);
	void Mul_iterations(int, int, int);
	void Karatsuba_iterations(int, int, int);
	void Barret_iterations(int, int, int);
	void Montgomery_iterations(int, int, int);
	void Euclides_iterations(int, int, int);
	void Magma_print(DATATYPE *, DATATYPE *, DATATYPE *, char, int, int);
	void Magma_print_mod(DATATYPE *, DATATYPE *, DATATYPE *, DATATYPE *, int);

    #define MPLIB_H

#endif /* MPLIB_H */
