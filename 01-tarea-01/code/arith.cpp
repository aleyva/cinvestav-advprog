#include <stdio.h>		// printf y scanf, NULL
#include <stdlib.h>		// rand, srand, malloc, free
#include <inttypes.h>   // __uint128_t
#include <math.h> 		// ceil
#include <string.h>     // memcpy
#include "mplib.h"


void MP_add(const DATATYPE *num1, const DATATYPE *num2, DATATYPE *add_res, int wsize){ 
    int carry = 0;    
    int i;    
    for(i = 0; i < wsize; i++){
        add_res[i] = num1[i] + num2[i] + carry;
        if(add_res[i] < num1[i]) carry = 1;
        else carry = 0; //si el valor de la entrada i del resultado es menor que la del primer sumando        
    }     
    add_res[i] = carry;
}

void MP_add_nocarry(const DATATYPE *num1, const DATATYPE *num2, DATATYPE *add_res, int wsize){
    int carry = 0,carry_temp=0;     //declaracion del carry igual a cero    
    for(int i = 0; i <= wsize; i++){  //para i desde 0 hasta word size-1
        add_res[i] = (num1[i] + num2[i]); //entrada i de res es igual a la suma de las entradas de los sumandos + el acarreo
        carry_temp = carry;
        carry = 0;
        if(add_res[i] < num1[i]){
            carry = 1;
        }else{
            carry = 0; //si el valor de la entrada i del resultado es menor que la del primer sumando
        }
        add_res[i] += carry_temp;
    }                                                   //el valor del acarreo sera 1    
}

//the function substract ALWAYS num2 from num1
void MP_sub(const DATATYPE *num1, const DATATYPE *num2, DATATYPE *sub_res, int wsize){
    int carry = 0;
    for(int i = 0; i < wsize; i++){
        sub_res[i] = num1[i] - num2[i] - carry;
        if(num1[i] < num2[i]) carry = 1;
        else carry = 0;
    }
    //si carry = 1, entonces el resultado es negativo.
}


void MP_sub_inv_add(DATATYPE *num1, DATATYPE *num2, DATATYPE *sub_res, int wsize){
    DATATYPE *num2_inv_add, *one, *not_num2;
    MP_init_arr_with_zeros(&num2_inv_add, wsize);
    MP_init_arr_with_zeros(&one, wsize);
    MP_init_arr_with_zeros(&not_num2, wsize);
    one[0]=1;  
    for (int i = 0; i < wsize; i++) {
      not_num2[i] = ~num2[i];
    }
    MP_add(not_num2, one, num2_inv_add, wsize);
    MP_add(num1, num2_inv_add, sub_res, wsize); 
}

/*
void MP_sub_signed(const DATATYPE *num1, const DATATYPE *num2, DATATYPE *sub_res, int wsize){
    __int128_t *sub_res_temp[wsize];
    int carry = 0;
    for(int i = 0; i < wsize; i++){
        sub_res_temp[i] = num1[i] - num2[i] - carry;
        if(num1[i] < num2[i]) carry = 1;
        else carry = 0;
    }
    //sub_res_temp *= -1;
    //si carry = 1, entonces el resultado es negativo.
    print
}*/

void MP_mul(DATATYPE *num1, DATATYPE *num2, DATATYPE *mul_res, int wsize){
    int i, j;    //indices i,j
    DATATYPE low_part, high_part, line_carry;  //declaracion de tipo short de la parte baja, alta de la multi. y el acarreo de renglones
    DOUBLEDATATYPE double_mult;                //de la mult. y declaramos el double_mult

    for(i = 0; i < wsize; i++){   //para i desde 0 hasta word size-1
        high_part = 0;
        double_mult = 0;
        for(j = 0; j < wsize; j++){
            line_carry = 0;                        
            double_mult = (DOUBLEDATATYPE)(num1[i]) * (DOUBLEDATATYPE)(num2[j]);   //multiplicacion de la entrada i de num1 con num2
            low_part = double_mult & FMASKDATATYPE;  //enmascaramos para quedarnos con la parte baja del resultado

            mul_res[i+j] += low_part;      //a la entrada i+j del resultado se le suma la parte baja
            if (mul_res[i+j] < low_part)   //y en caso de ser menor que dicha parte baja
                    line_carry += 1;     //al acarreo del renglon se le suma 1

            mul_res[i+j] += high_part;   //a la entrada i+j del resultado se le suma la parte alta
            if (mul_res[i+j] < high_part)  //y en caso de ser menor que dicha parte alta
                    line_carry += 1;    //al acarreo del renglon se le suma 1

            high_part = (double_mult >> ( sizeof(DATATYPE) * 8 )) + line_carry;	//a la parte alta se le asigna el valor de double_mult con corrimiento de 16 bits a la

        }                
        mul_res[wsize+i] = high_part;
    }
}

//algoritmo karatsuba
/*
Multiplicar x*y
x = 5678
y = 1234
a = parte mas significativa de x
b = parte menos significativa de x
c = parte mas significativa de y
d = parte menos significativa de y

step 1: computar recursivamente a*c
step 2: computar recursivamente b*d
step 3: computar recursivamente (a+b)(c+d)
step 4: computar resultado paso 3 - paso 2 - paso 1
step 5: (10^n)ac + (10^n/2)(ad+bc)+bd
*/

//ALERTA: karatsuba solo funciona para palabras con potencias de 2
void MP_karatsuba(DATATYPE *num1, DATATYPE *num2, DATATYPE *mul_res, int words){

    if( (words / 2) >= 1){
		
        int half_words = ceil((float)words/2);
        int shift;
        
        

        //se definen punteros de arreglos		
        DATATYPE *term1, *term2, *term3, *mul_res_temp;
        DATATYPE *step1, *step2, *step3, *step4, *step5;
        DATATYPE *add;
        DATATYPE *num1_temp, *num2_temp;

        //inicializar los vectores		
        MP_init_arr_with_zeros(&term1, words*2);
        MP_init_arr_with_zeros(&term2, words*2);
        MP_init_arr_with_zeros(&term3, words*2);        
        MP_init_arr_with_zeros(&mul_res_temp, words*2);
        MP_init_arr_with_zeros(&step1, words);
        MP_init_arr_with_zeros(&step2, words);
        MP_init_arr_with_zeros(&step3, words);
        MP_init_arr_with_zeros(&step4, words);
        MP_init_arr_with_zeros(&step5, words);		        
        MP_init_arr_with_zeros(&add, words+1);

        //step 1: computar recursivamente a*c (partes altas)
        MP_init_arr_with_zeros(&num1_temp, half_words);
        MP_init_arr_with_zeros(&num2_temp, half_words);
        memcpy(num1_temp, num1+half_words, half_words * sizeof(DATATYPE));
        memcpy(num2_temp, num2+half_words, half_words * sizeof(DATATYPE));        
        MP_karatsuba(num1_temp, num2_temp, step1, half_words);        
        free(num1_temp);
        free(num2_temp);

        //step 2: computar recursivamente b*d (partes bajas)        
        MP_init_arr_with_zeros(&num1_temp, half_words);
        MP_init_arr_with_zeros(&num2_temp, half_words);
        memcpy(num1_temp, num1, half_words * sizeof(DATATYPE));
        memcpy(num2_temp, num2, half_words * sizeof(DATATYPE));        
        MP_karatsuba(num1_temp, num2_temp, step2, half_words);        
        free(num1_temp);
        free(num2_temp);

        //step 3: computar recursivamente a*d (partes cruzadas)        
        MP_init_arr_with_zeros(&num1_temp, half_words);
        MP_init_arr_with_zeros(&num2_temp, half_words);
        memcpy(num1_temp, num1+half_words, half_words * sizeof(DATATYPE));
        memcpy(num2_temp, num2, half_words * sizeof(DATATYPE));        
        MP_karatsuba(num1_temp, num2_temp, step3, half_words);        	
        free(num1_temp);
        free(num2_temp);

        //step 4: computar recursivamente b*c (partes cruzadas)        
        MP_init_arr_with_zeros(&num1_temp, half_words);
        MP_init_arr_with_zeros(&num2_temp, half_words);
        memcpy(num1_temp, num1, half_words * sizeof(DATATYPE));
        memcpy(num2_temp, num2+half_words, half_words * sizeof(DATATYPE));        
        MP_karatsuba(num1_temp, num2_temp, step4, half_words);        	
        free(num1_temp);
        free(num2_temp);

        //step 5: (ad+bc)		        
        MP_add(step3, step4, add, words); //suma (ad+bc)        
        
        //step 6: (10^n)ac y (10^n/2)(ad+bc) + bd        
        if(words % 2 == 0){
            shift = words;
        }else{
            shift = words+1;
        }
        MP_shift_array_wsize_right(step1, words, shift, term1, words*2);
        MP_shift_array_wsize_right(add, words+1, (shift/2), term2, words*2);
        MP_shift_array_wsize_right(step2, shift, 0, term3, words*2);        
                
        //step 7: (10^n)ac y (10^n/2)(ad+bc) + bd              
        MP_add_nocarry(term1, term2, mul_res_temp, words*2); //suma (ad+bc)
        MP_add_nocarry(mul_res_temp, term3, mul_res, words*2); //suma (ad+bc)
         /*
        try{
            //free all the variables		
            free(add);
            free(step1); free(step2); free(step3); free(step4); free(step5);
            free(term1); free(term2); free(term3); free(mul_res_temp);
        }catch (int e){
                printf("\nNo se pudo liberar la memoria de los arreglos en karatsuba");
        }*/
		        
    }else{		
	//multiplicacion tradicional
        DATATYPE low_part;
	DOUBLEDATATYPE double_mult;
                               
        double_mult = (DOUBLEDATATYPE)num1[0] * (DOUBLEDATATYPE)num2[0];   
        low_part = double_mult & FMASKDATATYPE;  	//enmascaramos para quedarnos con la parte baja del resultado
        
        mul_res[0] = low_part;
        mul_res[1] = (double_mult >> ( sizeof(DATATYPE) * 8 ));	
    }
}


void MP_barret_red(DATATYPE *num1, DATATYPE *num2, DATATYPE *prime, DATATYPE *mu, DATATYPE *res_bar, int wsize){	 
	
	DATATYPE *num_res, *res_temp, *res_mu, *res_qh, *res_qh_p, *num_res_mod, *qh_p_mod, *prime_h;
    DATATYPE *prime_wsize_add1, *res_bar_wsize_add1; //variables definidas por leyva
    
	MP_init_arr_with_zeros(&num_res, wsize*2);        
    MP_init_arr_with_zeros(&res_temp, wsize+1);	
	MP_init_arr_with_zeros(&res_mu, (wsize+1)*2);
	MP_init_arr_with_zeros(&res_qh, wsize+1);
    MP_init_arr_with_zeros(&prime_h, wsize+1);
	MP_init_arr_with_zeros(&res_qh_p, (wsize+1)*2);        
	MP_init_arr_with_zeros(&num_res_mod, wsize+1);
	MP_init_arr_with_zeros(&qh_p_mod, wsize+1);        
    MP_init_arr_with_zeros(&res_bar_wsize_add1, wsize+1);

    //z:= x*y;  z = wsize*2
    //la siguiente bifurcacion se genera por el caso especial cuando montgomery necesita numeros en dominio de barret
    //y para ello multiplica los numeros aleatorios por una palabra con el bit mas significativo, pero esta palabra es de wsize+1
    //esto genera incompatibilidad de wsize de variables al momento de multiplicar; una es de wzise y otra es de wsize+1
    //el else es el caso ordinario, que no tiene que ver con montgomery
    if(MP_is_one(num2,wsize) == 1){
        for(int i=0; i<wsize; i++){
            num_res[wsize+i] = num1[i];        
        }
    }else{
        MP_mul(num1, num2, num_res, wsize);
    }
            
    // reducción
    //qh := ((z div b^(W-1))*mu) div b^(W+1);
    memcpy(res_temp, num_res + (wsize-1), (wsize+1) * sizeof(DATATYPE));
	MP_mul(res_temp, mu , res_mu, wsize+1);        
	memcpy(res_qh, res_mu + (wsize+1), (wsize+1) * sizeof(DATATYPE));   
        
    //hasta aqui va bien        
    //z mod (b^(W + 1))
    memcpy(num_res_mod, num_res, (wsize+1) * sizeof(DATATYPE));        
    //(qh*p) mod (b^(W+1));
    memcpy(prime_h, prime, wsize * sizeof(DATATYPE)); 
    MP_mul(res_qh, prime_h , res_qh_p, wsize+1); //el prime tiene una palabra menos
    memcpy(qh_p_mod, res_qh_p, (wsize+1) * sizeof(DATATYPE));        
    //rs := z mod (b^(W + 1)) - (qh*p) mod (b^(W+1));
    MP_sub(num_res_mod, qh_p_mod, res_bar_wsize_add1, wsize+1);
    	
    /*while (rs ge p) do
        rs := rs -p;	
    end while;*/      
	
	while (MP_is_greater_or_equal_than(res_bar_wsize_add1, prime_h, wsize + 1 ) == 1){            
            MP_sub(res_bar_wsize_add1, prime_h, res_bar, wsize + 1);		
            res_bar_wsize_add1 = res_bar;                                    
	}        
    memcpy(res_bar, res_bar_wsize_add1, wsize * sizeof(DATATYPE));
        
        /*
        printf("\nprime\np := 0x"); MP_print_arr_LtoR(prime, wsize);
        printf("\nmu := 0x"); MP_print_arr_LtoR(mu, wsize+1);	
        printf("\nx := 0x"); MP_print_arr_LtoR(num1, wsize);
        printf("\ny := 0x"); MP_print_arr_LtoR(num2, wsize);
        printf("\nnum_res: "); MP_print_arr_LtoR(num_res, wsize*2);	
        printf("\nres_temp: "); MP_print_arr_LtoR(res_temp, wsize+1);	
        printf("\nres_mu: "); MP_print_arr_LtoR(res_mu, (wsize+1)*2);
        printf("\nres_qh: "); MP_print_arr_LtoR(res_qh, wsize+1);        
        printf("\nres_qh_p: "); MP_print_arr_LtoR(res_qh_p, (wsize+1)*2);
        printf("\nnum_res_mod: "); MP_print_arr_LtoR(num_res_mod, wsize+1);
        printf("\nqh_p_mod: ");	MP_print_arr_LtoR(qh_p_mod, wsize+1);
        printf("\nres_bar: "); MP_print_arr_LtoR(res_bar, wsize+1);
        printf("\nres_bar_wsize_add1: "); MP_print_arr_LtoR(res_bar_wsize_add1, wsize+1);
        */
}

void MP_montgomery(DATATYPE *num1, DATATYPE *num2, DATATYPE *prime, DATATYPE *pinv, DATATYPE *res_mon, int wsize){
    
    DATATYPE *c, *q_temp, *q, *r_temp, *r, *z, *temp_res;    
    
    MP_init_arr_with_zeros(&c, wsize*2);
    MP_init_arr_with_zeros(&q_temp, wsize*3);
    MP_init_arr_with_zeros(&q, wsize);
    MP_init_arr_with_zeros(&r_temp, wsize*2);    
    MP_init_arr_with_zeros(&r, (wsize*2)+1);
    MP_init_arr_with_zeros(&z, wsize+1);
    MP_init_arr_with_zeros(&temp_res, wsize);
    
    //c := x*y; 
    MP_mul(num1, num2, c, wsize);    
    //q_temp := c*pinv; 
    MP_mul(c, pinv, q_temp, wsize*2);
    //q := q_temp mod R; //copiar solo las palabras de wsize    r
    memcpy(q, q_temp, wsize * sizeof(DATATYPE));
    //r_temp := q*p;     
    MP_mul(q, prime, r_temp, wsize);
    //r := c + r_temp;     
    MP_add(c, r_temp, r, (wsize*2));    
    //z := r div R; 
    memcpy(z, r + wsize, (wsize+1) * sizeof(DATATYPE));
         
    //if (z >= p) then
    //        z := z -p;
    //end if;
    //verifica si z tiene carry, entonces z es mayor
    if(MP_is_greater_or_equal_than(temp_res, prime, wsize)==1){
        //ALERTA, no estoy tomando en cuenta para la resta la palabra mas alta
        //de "z", que tendría 1, si generó carry de la suma.
        MP_sub(z, prime, res_mon, wsize+1); 
    }else{
        memcpy(res_mon, z, wsize * sizeof(DATATYPE));
    }
    
    /*
    printf("\nc: "); MP_print_arr_LtoR(c, wsize*2);
    printf("\nr_temp: "); MP_print_arr_LtoR(r_temp, wsize*2);
    printf("\nq: "); MP_print_arr_LtoR(q, wsize);
    printf("\nr: "); MP_print_arr_LtoR(r, (wsize*2)+1);
    printf("\nz: "); MP_print_arr_LtoR(z, wsize+1);
    printf("\nres: "); MP_print_arr_LtoR(res_mon, wsize);
    */
}

void MP_inverso_AEE(DATATYPE *num1, DATATYPE *primo, DATATYPE *inverso, int wsize){
		
	DATATYPE *x_1, *x_2, *x_1_temp, *x_2_temp; 
	DATATYPE *u, *v, *u_temp, *v_temp;
        

	MP_init_arr_with_zeros(&x_1, wsize);
	MP_init_arr_with_zeros(&x_2, wsize);	
	MP_init_arr_with_zeros(&u, wsize);
	MP_init_arr_with_zeros(&v, wsize);	
	MP_init_arr(&x_1_temp, wsize+1);
	MP_init_arr(&x_2_temp, wsize+1);
	MP_init_arr(&u_temp, wsize);
	MP_init_arr(&v_temp, wsize);
	x_1[0] = 1;

	memcpy(u, num1, wsize * sizeof(DATATYPE));
	memcpy(v, primo, wsize * sizeof(DATATYPE));

        /*int cont = 0, dos;
	printf("Valores iniciales: ");
	printf(" num1:"); MP_print_arr_LtoR(num1, wsize);
	printf(" primo:"); MP_print_arr_LtoR(primo, wsize);
	printf(" x_1: "); MP_print_arr_LtoR(x_1, wsize);
	printf(" x_2: "); MP_print_arr_LtoR(x_2, wsize);
	printf(" u:");	MP_print_arr_LtoR(u, wsize);
	printf(" v:");	MP_print_arr_LtoR(v, wsize);  
        */
	
	while( MP_is_one(u, wsize) == 0 && MP_is_one(v, wsize) == 0 )
	{       
            
            
		MP_fill_arr_with_zeros (x_1_temp, wsize+1);
		MP_fill_arr_with_zeros (x_2_temp, wsize+1);
		MP_fill_arr_with_zeros (u_temp, wsize);
		MP_fill_arr_with_zeros (v_temp, wsize);
		
            /*if(cont == 13){
                dos=cont+1;
            }*/
                
		while(MP_is_even(u) == 1){  //si num1 es par entra al while 
			MP_bit_shift_left(u, wsize); // num1 = num1/2
			
                        //ver si tenemos booleano de negativo
                        //sumar primo...
                        if(MP_is_even(x_1) == 1)		
			{   
				MP_bit_shift_left(x_1, wsize);   //probar si un numero es par
			} else  {
				//no se puede mandar como resultado algunos de los sumando
				//la suma arroja wsize+1
				MP_add(x_1, primo, x_1_temp, wsize);
				MP_bit_shift_left(x_1_temp, wsize);
				memcpy(x_1, x_1_temp, wsize * sizeof(DATATYPE));
			}		 
		}
		
		while(MP_is_even(v) == 1){
			MP_bit_shift_left(v, wsize);	
			if(MP_is_even(x_2) == 1){ 
				MP_bit_shift_left(x_2, wsize);
			} else{ 
				MP_add(x_2, primo, x_2_temp, wsize);
				MP_bit_shift_left(x_2_temp, wsize);
				memcpy(x_2, x_2_temp, wsize * sizeof(DATATYPE));
			}
		}

		if(MP_is_greater_or_equal_than(u, v, wsize) == 1){
            MP_sub_inv_add(u, v, u, wsize);
            MP_sub_inv_add(x_1, x_2, x_1, wsize);
		} else {		
            MP_sub_inv_add(v, u, v, wsize);
            MP_sub_inv_add(x_2, x_1, x_2, wsize);                    
		} 
		
		 		
		/*
                printf("\nvalores nuevos: ");
                printf(" #-%d ", cont); 
                printf(" x_1: "); MP_print_arr_LtoR(x_1, wsize);
		printf(" x_2: "); MP_print_arr_LtoR(x_2, wsize);		
                printf(" u:");	MP_print_arr_LtoR(u, wsize);
		printf(" v:");	MP_print_arr_LtoR(v, wsize);                
                cont++;
                */
	} 

        /*printf("\ninverso x_1: "); MP_print_arr_LtoR(x_1, wsize);
        printf("\ninverso x_2: "); MP_print_arr_LtoR(x_2, wsize);
        printf("\n");*/
	if( MP_is_one(u, wsize) == 1 ){
            memcpy(inverso, x_1, wsize * sizeof(DATATYPE));            
	}

	if(MP_is_one(v, wsize) == 1 ){			
            memcpy(inverso, x_2, wsize * sizeof(DATATYPE));		
	}		
}
