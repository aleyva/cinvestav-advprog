# -*- coding: utf-8 -*-


'''=============================================================================
 |   Assignment:  Proyecto Final
 |       Author:  Lisa Pal, Antonio Leyva
 |     Language:  Python
 |   To Compile:  python ga_addition_chain.py [args]
 |        Class:  Programación Avanzada
 |   Instructor:  Francisco Rodríguez Henríquez 
 |     Due Date:  2018-Dic-10
 +-----------------------------------------------------------------------------
 |
 |  Description:  This program find a quasi optimal addition chain with genetic algorithm
 |         Args:  --exp             # Exponent to reach in the addition chain > 2
 |                --pop_size        # Population size in each generation
 |                --max_gen         # Maximum number of generations
 |                --rep_gen         # Break if number of generations stuck with same fittest length value
 |                --pc              # Probability of Crossover (Pc)
 |                --pm              # Probability of Mutation (Pm)
 |                --pe              # Probability of Elitm (Pe)
 |                --er              # Elitism rate
 |                --ds_rate         # Double Stepping Rate
 |                --last2_rate      # Last two rate
 |                --n_mutants       # Number of mutants form child
 |       Output:  a quasi optimal addition chain
 |    Algorithm:  Implemented with genetic algorithms
 |
 |   Required Features Not Included:  python
 |   Known Bugs:  none
 |
 *==========================================================================='''

import sys
import getopt
import random
import time
import math

# Global parameters with default values
exp = 1000              # Exponent to reach in the addition chain
pop_size = 400          # Population size in each generation
max_gen = 500           # Maximum number of generations
rep_gen = 40            # Break if number of generations stuck with same fittest length value
Pc = .4                 # Probability of Crossover (Pc)
Pm = .7                 # Probability of Mutation (Pm)
Pe = 0                  # Probability of Elitm (Pe)
Er = .05                # Elitism rate
ds_rate = .5            # Double Stepping Rate
last2_rate = .4         # Rate for addition the last two elements in chain
n_mutants = 4           # Number of mutants from child



def print_chain(chain):
    str_chain = [str(item) for item in chain]
    print(", " . join(str_chain))

def print_population(population):
    length = len(population)
    for i in range(0,length):
        print population[i]

def fittest_chain(population):
    population.sort(key = lambda s: len(s))  
    return population[0]

def is_valid_chain(chain):
    last = len(chain)-1
    if(chain[last] == exp):
        for i in range(last-1,1,-1):
            difference = chain[i]-chain[i-1]
            try:
                chain.index(difference)
            except ValueError:
                print "difference: ", difference
                print "index: ", i
                return False
    else:
        return False
    return True

def probability(rate):
    rand = random.random()    
    if(rand < rate):
        return True
    else:
        return False

# Input: An invalid or incomplete addition chain 
#       (chain = chain[0], chain[1], ..., chain[l]. chain[l] = exp)
# Output: A feasible addition chain for exponent 'exp', with chain[last] = exp
def repair_and_fill(chain):    
    # remove invalid elements greater than exponent
    last = len(chain)-1
    while (chain[last] > exp):
        chain.pop(last)
        last-=1    
    
    # chain[last] defines the last element of the addition chain
    while (chain[last] != exp):
        rand = last-2
        idx2 = 0
        if probability(ds_rate):
            # i. With the probability of 'ds_rate', 
            # select the last element of the chain for double it
            idx2 = last            
        elif probability(last2_rate):
            # ii. With the probability of 'last2_rate', 
            # select the previous of last element of the chain
            idx2 = last-1            
        else:
            # iii. Select random element from 0 to last-2
            idx2 = random.randint(0, last-2) 
        # Append the new element
        chain.append(chain[last] + chain[idx2])            
        
        # If the last element added is larger than exponent, 
        # remove it and seach for a new valid element, 
        # starting a sequential from last random element selected 
        # to zero element        
        if chain[last+1] > exp:
            chain.pop(last+1)
            for idx in range(rand-1,-1,-1):
                if(chain[last] + chain[idx] <= exp):
                    chain.append(chain[last] + chain[idx])
                    break            
        last += 1                
    return chain

# Input: Two parents, two crossover points and common length between two parents
# Output: A new born child
def gestate_crossover(parent1, parent2, crossover_point1, crossover_point2, length):
    # Generate first elements of 'child' by copying from 'parent1'
    child = parent1[0:crossover_point1+1]            
    # Generate between elements of 'child' by using rules from 'parent2'   
    for last in range(crossover_point1, crossover_point2):
        # Look for position that define the rule for actual element,
        # such that parent2[i] = parent2[a] + parent2[b]
        idx = parent2.index(parent2[last+1]-parent2[last])                
        child.append(child[last] + child[idx])        
    
    # Generate last elements of 'child' by using rules from 'parent1'    
    for last in range(crossover_point2, length-1):
        # Look for positions that define the rule for actual element,
        # such that parent1[i] = parent1[a] + parent1[b]
        idx = parent1.index(parent1[last+1]-parent1[last])                
        child.append(child[last] + child[idx])        
       
    return child    

# Input: Parents parent1 and paretn2 to be reproduced
# Output: Two new children, child1 and child2
def crossover(parent1, parent2):    
    if probability(Pc):
        # Set a common length for both parents
        length = len(parent1)
        length_p2 = len(parent2)
        if(length_p2 < length):
            length = length_p2
        # Set a 'mid_length' from common length
        mid_length = length//2
        # Choose first and second crossover points
        crossover_point1 = random.randint(2, mid_length)        
        crossover_point2 = random.randint(mid_length +1, length - 2)        
        
        # Gestate 'child1' and 'child2'
        child1 = gestate_crossover(parent1, parent2, crossover_point1, crossover_point2, length)        
        child2 = gestate_crossover(parent2, parent1, crossover_point1, crossover_point2, length)

        # Repair and fill 'child1' and 'child2' to build feasible chains
        child1 = repair_and_fill(child1)
        child2 = repair_and_fill(child2)
        
    else:
        # Copy 'parent1' and 'parent2' in 'child1' and 'child2'
        child1 = parent1[:]                
        child2 = parent2[:]   
    return [child1, child2]

# Input: A child to be mutated
# Output: All the mutated child versions
def mutation(child):
    mutants = [] 
    if probability(Pm):                              
        # Select a common mutation
        limit_idx = random.randint(2, len(child)-1) 
        # Generate N mutants of the child
        for mutant_idx in range(0,n_mutants):            
            # Select a random position idx where idx = random(0, limit_idx − 1) is a
            # random element of the actual clone.
            idx = random.randint(0, limit_idx-1)          
            # Generate first elements of mutant by copying from child
            mutant = child[0:limit_idx+1]
            # Generate a new element for mutant[last] (mutation point), such
            # that mutant[last] = mutant[limit_idx] + mutant[idx]                      
            mutant.append(mutant[limit_idx] + mutant[idx])
            # Fill the mutant addition chain starting from 'limit_idx'
            repair_and_fill(mutant)
            # Add the new 'mutant' to 'mutants'
            mutants.append(mutant)
    return mutants

def elitism(population):
    elites = []
    if probability(Pe):           
        elite_no = int(math.ceil(len(population)*Er))        
        population.sort(key = lambda s: len(s))
        elites = population[0:elite_no]        
    return elites

# Input: Exponent exp
# Output: A valid addition (chain = chain[0], chain[1], ..., chain[l]. chain[l] = exp)
def init_chain():    
    global exp
    # Set chain[0] = 1 and chain[1] = 2
    chain = [1,2]     
    # Assign to chain[2], chain[3], chain[4], random valid values
    # such that chain[last] = chain[a] + chain[b]
    for last in range(1, 4):
        chain.append(chain[last] + chain[random.randint(0,last)])        
    chain = repair_and_fill(chain)    
    return chain

def get_args(sysargs):
    try:	
        global exp, pop_size, max_gen, Pc, Pm, Er, ds_rate, select_rate, n_mutants
        opts, args = getopt.getopt(sysargs, "", ("exp=", "pop_size=", "max_gen=", "pc=", "pm=", "er=", "ds_rate=", "select_rate=", "n_mutants=", "rep_gen="))		
        for opt,arg in opts:
            if opt == "--exp":
                exp = int(arg)
            elif opt == '--pop_size':
                pop_size = int(arg)
            elif opt == '--max_gen':
                max_gen = int(arg)
            elif opt == '--pc':
                Pc = float(arg)
            elif opt == '--pm':
                Pm = float(arg)
            elif opt == '--er':
                Er = float(arg)
            elif opt == '--ds_rate':
                ds_rate = float(arg)			
            elif opt == '--select_rate':
                select_rate = float(arg)			
            elif opt == '--n_mutants':
                n_mutants = int(arg)
            elif opt == '--rep_gen':
                rep_gen = int(arg)
                	
    except:
        print('Arguments parser error, try -h' + arg)		
        sys.exit(2)

# Input: An exponent 'exp', population size 'pop_size' and the number of maximun geneartions 'max_gen'
# Output: A quasi optimal addition chain (chain = chain[0], chain[1], ..., chain[l]. chain[l] = exp)
def ga_addition_chain():    
    global pop_size, max_gen, exp
    # Setting up initial population    
    population = []
    for i in range(0, pop_size):
        population.append(init_chain())    

    # Obtain the length of fittest chain in first population
    population.sort(key = lambda s: len(s))
    len_fittest = len(population[0])
    repetition = 0

    for i in range(1, max_gen):        
        next_generation = []
        print "Generation ", i+1
        for pop_idx in range(0, pop_size):                   
            # Generate two new children by applying crossover operator        
            child1, child2 = crossover(population[pop_idx], population[random.randint(0, pop_idx)])            
            # Generate two new children and add to next_generation
            next_generation.append(child1)
            next_generation.append(child2)
            # Apply the mutation operator to each new created child and add the mutants to next_generation
            next_generation.extend(mutation(child1))
            next_generation.extend(mutation(child2))
            
        
        # Apply the elitism operator
        next_generation.extend(elitism(population))        
        
        # Replace population with fittest new_generation chains (only 'pop_size' of them)
        next_generation.sort(key = lambda s: len(s))
        population = next_generation[0:pop_size]

        # break condition if the length fittest value stuck in n generations
        if(len_fittest == len(population[0])):
            repetition +=1
            if(repetition == rep_gen):
                break
        else:
            len_fittest = len(population[0])
            repetition = 0
        
    # Report fittest
    print "\n--- FITTEST ---"
    fittest = fittest_chain(population)
    print "Length:", len(fittest), "\tIs valid chain? ", is_valid_chain(fittest)
    print fittest    
    
# main
start = time.time()
get_args(sys.argv[1:])
ga_addition_chain()
end = time.time()
print "\nTotal time: ", (end - start)

 



# --- DEPRECATED ---

# def init_population(population):    
#     global pop_size
#     no_items = 5    
#     chain = [[1, 2]]
#     for i in range(0, no_items):
#         temp = []
#         for items in chain:
#             if(len(chain) < no_items+2):
#                 for item in items:
#                     p = items[:]
#                     p.append(items[-1]+item)
#                     temp.append(p)
#             else:
#                 for item in items:
#                     p = items[:]
#                     p.append(items[-1]+item)                    
#                     population.append(repair_and_fill(p))
#                     population.append(repair_and_fill(p))
#         chain = temp[:]
    
#     pop_size = len(population)