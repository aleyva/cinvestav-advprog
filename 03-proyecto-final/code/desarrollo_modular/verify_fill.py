# -*- coding: utf-8 -*-
import random



# Global constants
pop_size = 200          # Population Size
max_gen = 300           # Maximum number of generations
Pc = .004               # Crossover Rate (Pc)
Pm = .01                # Mutation Rate (Pm)
ds_rate = .7            # Double Stepping Rate
select_rate = .2        # Probability of selecting
n_mutants = 4           # utilized in mutation operator = 4


def flip(rate):
    rand = random.random()    
    if(rand > rate):
        return True
    else:
        return False

def print_chain(chain):
    str_chain = [str(item) for item in chain]
    print(", " . join(str_chain))

def print_population(population):
    length = len(population)
    for i in range(0,length):
        print_chain(population[i])


# Input: An Incomplete Addition Chain (chain = u1 , u2 , ..., ul. ul = e,)
        # where last+1 represents the next position to be filled.
# Output: A feasible addition chain for Exponent e, with last l
def fill(chain, last, exp):    
    # exception from mutation code
    if chain[last] > exp:
        chain.pop(last)
        last -= 1
    
    # chain[last] defines the last element of the addition chain
    while (chain[last] != exp):
        rand = last
        if flip(ds_rate):
            # Applying Double Stepping(DS)
            chain.append(2*chain[last])
        elif flip(select_rate):
            # Selecting the last two elements
            chain.append(chain[last] + chain[last-1])
        else:
            # Selecting a random element
            rand = random.randint(0, last) 
            chain.append(chain[last] + chain[rand])            
        
        if chain[last+1] > exp:
            chain.pop(last+1)
            for i in range(rand-1,-1,-1):
                if(chain[last] + chain[i] <= exp):
                    chain.append(chain[last] + chain[i])
                    break            
        last += 1        
        # print_chain(chain)

# Input: Exponent exp
# Output: A valid addition chain = u1 , u2 , ..., ul = e with length l
def init_chain(exp):    
    chain = [1,2]    
    chain.append(2 + random.randint(1,2))        
    fill(chain, len(chain)-1, exp)
    print_chain(chain)
    return chain



exp = 743
init_chain(exp)