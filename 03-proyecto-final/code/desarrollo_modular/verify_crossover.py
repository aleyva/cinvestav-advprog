# -*- coding: utf-8 -*-
import random



# Global constants
pop_size = 200          # Population Size
max_gen = 300           # Maximum number of generations
Pc = .004               # Crossover Rate (Pc)
Pm = .01                # Mutation Rate (Pm)
ds_rate = .7            # Double Stepping Rate
select_rate = .2        # Probability of selecting
n_mutants = 4           # utilized in mutation operator = 4


def flip(rate):
    rand = random.random()    
    if(rand > rate):
        return True
    else:
        return False

def print_population(population):
    length = len(population)
    for i in range(0,length):
        print population[i]


def fittest_chain(population):
    fittest = population[0]
    fittest_length = len(population[0])
    length = len(population)
    for i in range(1,length):
        if(len(population[i]) < fittest_length):
            fittest = population[i]
            fittest_length = len(population[i])
    return fittest

# Input: An Incomplete Addition Chain (chain = u1 , u2 , ..., ul. ul = e,)
        # where last+1 represents the next position to be filled.
# Output: A feasible addition chain for Exponent e, with last l
def fill(chain, last, exp):    
    # exception from mutation and repair for crossover code
    while (chain[last] > exp):
        chain.pop(last)
        last-=1    
    
    # chain[last] defines the last element of the addition chain
    while (chain[last] != exp):
        rand = last
        if flip(ds_rate):
            # Applying Double Stepping(DS)
            chain.append(2*chain[last])
        elif flip(select_rate):
            # Selecting the last two elements
            chain.append(chain[last] + chain[last-1])
        else:
            # Selecting a random element
            rand = random.randint(0, last) 
            chain.append(chain[last] + chain[rand])            
        
        if chain[last+1] > exp:
            chain.pop(last+1)
            for i in range(rand-1,-1,-1):
                if(chain[last] + chain[i] <= exp):
                    chain.append(chain[last] + chain[i])
                    break            
        last += 1        
        # print_chain(chain)
    return chain

# def gestate_crossover(parent1, parent2, crossover_point1, crossover_point2, length):
#     # Generate child elements by using rules from parent1    
#     child = parent1[0:crossover_point1+1]        
#     print "child:               ", child
#     # Generate child elements by using rules from parent2    
#     for last in range(crossover_point1, crossover_point2):
#         # Look for positions that define the rule for actual element,
#         # such that parent2[i] = parent2a + parent2b
#         idx = parent2.index(parent2[last+1]-parent2[last])                
#         child.append(child[last] + child[idx])        
        
#     print "child_cross_parent1: ", child
    
#     for last in range(crossover_point2, length-1):
#         # Look for positions that define the rule for actual element,
#         # such that parent1[i] = parent1a + parent1b
#         idx = parent1.index(parent1[last+1]-parent1[last])                
#         child.append(child[last] + child[idx])        

#     print "child_cross_parent2: ", child
#     # If child is not a valid addition chain, apply repair process to Child2

#     return child    

def gestate_crossover(parent1, parent2, crossover_point1, crossover_point2, length):
    # Generate child elements by using rules from parent1    
    child = parent1[0:crossover_point1+1]            
    # Generate child elements by using rules from parent2    
    print "child:               ", child           
    for last in range(crossover_point1, crossover_point2):
        # Look for positions that define the rule for actual element,
        # such that parent2[i] = parent2a + parent2b
        idx = parent2.index(parent2[last+1]-parent2[last])                
        child.append(child[last] + child[idx])        
    
    print "child_cross_parent1: ", child
    for last in range(crossover_point2, length-1):
        # Look for positions that define the rule for actual element,
        # such that parent1[i] = parent1a + parent1b
        idx = parent1.index(parent1[last+1]-parent1[last])                
        child.append(child[last] + child[idx])        
    
    print "child_cross_parent2: ", child
    return child    



# Input: Parents P1 and P2 to be reproduced
# Output: Two new children, C1 and C2
def crossover(parent1, parent2, child1, child2, exp):
    if flip(Pc):
        # Set a common length length for both Parents (P1, P2)
        length = len(parent1)
        length_p2 = len(parent2)
        if(length_p2 < length):
            length = length_p2
        
        mid_length = length//2
        # Choose first crossover point
        crossover_point1 = random.randint(2, mid_length)
        # Choose second crossover point
        crossover_point2 = random.randint(mid_length +1, length - 2)
        
        print "cross_p1: ", crossover_point1
        print "cross_p2: ", crossover_point2
        print "length: ", length
        print "parent1:             ", parent1
        print "parent2:             ", parent2             
        print "----CHILD 1----"
        child1 = gestate_crossover(parent1, parent2, crossover_point1, crossover_point2, length)
        print "----CHILD 2----"
        child2 = gestate_crossover(parent2, parent1, crossover_point1, crossover_point2, length)

        #repair childs
        print "----CHILD 1 REPAIR----"
        child1 = fill(child1, len(child1)-1, exp)
        print child1

        print "----CHILD 2 REPAIR----"
        child2 = fill(child2, len(child2)-1, exp)
        print child2

    else:
        # child1 will become a copy of parent1
        child1 = parent1[:]        
        # child2 will become a copy of parent2
        child2 = parent2[:]        


# Input: Exponent exp
# Output: A valid addition chain = u1 , u2 , ..., ul = e with length l
def init_chain(exp):    
    chain = [1,2]    
    chain.append(2 + random.randint(1,2))        
    chain = fill(chain, len(chain)-1, exp)
    # print_chain(chain)
    return chain

# ga_addition_chain(743, 6, 3)
exp = 500
parent1 = init_chain(exp)
parent2 = init_chain(exp)
child1 = []
child2 = []
crossover(parent1, parent2, child1, child2, exp)
