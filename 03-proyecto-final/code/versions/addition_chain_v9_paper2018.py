# -*- coding: utf-8 -*-
import sys
import getopt
import random
import time
import math

# Global parameters
exp = 3167711
pop_size = 10         # population Size
max_gen = 50           # Maximum number of generations
Pc = .95               # Probability of Crossover (Pc)
Pm = .20                # Probability of Mutation (Pm)
Pe = 1                # Probability of Elitm (Pe)
Er = .05                  # Elitism rate
ds_rate = 3/5            # Double Stepping Rate
n_mutants = 4           # utilized in mutation operator = 4


def print_chain(chain):
    str_chain = [str(item) for item in chain]
    print(", " . join(str_chain))

def print_population(population):
    length = len(population)
    for i in range(0,length):
        print population[i]

def is_valid_chain(chain):
    last = len(chain)-1
    if(chain[last] == exp):
        for i in range(last-1,1,-1):
            difference = chain[i]-chain[i-1]
            try:
                idx=chain.index(difference)
            except ValueError:
                print "difference: ", difference
                print "index: ", i
                return False
    else:
        return False
    return True

def fittest_chain(population):
    fittest = population[0]
    fittest_length = len(population[0])
    length = len(population)
    for i in range(1,length):
        if(len(population[i]) < fittest_length):
            fittest = population[i]
            fittest_length = len(population[i])    
    return fittest

def probability(rate):
    rand = random.random()    
    if(rand < rate):
        return True
    else:
        return False

def repair_chain(chain):    
    # 1. Delete duplicate and order elements in the chain.
    chain = list(set(chain))
    
    # 2. Delete elements greater than the exp value.
    last = len(chain)-1    
    while (chain[last] > exp):
        chain.pop(last)
        last-=1  

    # 4. Ensure that the chain finishes with the exp value by repeating operations in the following order:
    # (a) Try to find two elements in the chain that result in exp.
    difference = exp-chain[last]
    idx = type(chain.index(difference))
    if (idx == int):
        chain.append(chain[last]+chain[idx])    
    
    while(chain[last] < exp):        
        # (b) Uniformly at random apply:    
        idx1 = last
        option = random.randint(1,3)                        
        if (option == 1):            
            # i. Double the last element of the chain while it is smaller than exp.
            idx2 = last              
        elif (option == 2):            
            # ii. Add the last element and a random element.
            idx2 = random.randint(0, last)                 
        elif (option == 3):
            # iii. Add two random elements.
            mid = last//2
            idx1 = random.randint(0, mid)
            idx2 = random.randint(mid+1, last)                        
        chain.append(chain[idx1] + chain[idx2])
        
        if chain[last+1] > exp:
            chain.pop(last+1)
            for i in range(last,-1,-1):
                if(chain[last] + chain[i] <= exp):
                    chain.append(chain[last] + chain[i])
                    break   
        last += 1 
    
    return chain

def gestate_crossover(parent1, parent2, crossover_point1, crossover_point2, length):
    # Generate child elements by using rules from parent1    
    child = parent1[0:crossover_point1+1]            
    # Generate child elements by using rules from parent2    
    for last in range(crossover_point1, crossover_point2):
        # Look for positions that define the rule for actual element,
        # such that parent2[i] = parent2a + parent2b
        idx = parent2.index(parent2[last+1]-parent2[last])                
        child.append(child[last] + child[idx])        
    
    for last in range(crossover_point2, length-1):
        # Look for positions that define the rule for actual element,
        # such that parent1[i] = parent1a + parent1b
        idx = parent1.index(parent1[last+1]-parent1[last])                
        child.append(child[last] + child[idx])        
       
    return child    

# Input: Parents P1 and P2 to be reproduced
# Output: Two new children, C1 and C2
def crossover(parent1, parent2):
    if probability(Pc):
        global exp
        # Set a common length length for both Parents (P1, P2)
        length = len(parent1)
        length_p2 = len(parent2)
        if(length_p2 < length):
            length = length_p2
        
        mid_length = length//2
        # Choose first crossover point
        crossover_point1 = random.randint(2, mid_length)
        # Choose second crossover point
        crossover_point2 = random.randint(mid_length +1, length - 2)        
        
        child1 = gestate_crossover(parent1, parent2, crossover_point1, crossover_point2, length)        
        child2 = gestate_crossover(parent2, parent1, crossover_point1, crossover_point2, length)

        #repair and fill children
        child1 = repair_chain(child1)
        child2 = repair_chain(child2)
        
    else:
        # child1 will become a copy of parent1
        child1 = parent1[:]        
        # child2 will become a copy of parent2
        child2 = parent2[:]   
    return [child1, child2]

    
# Input: A Child Individual child = (U, l).
# Output: The Best Mutated Child Version Cm = (U m , l)
def mutation(child):     
    if probability(Pm):
        global exp
        rand = random.randint(2, exp - 1)
        if random.randint(0, 1) == 1:
            child.append(child[rand-1] + child[rand-2])
        else:
            rand3 = random.randint(2, rand - 1)                    
            child.append(child[rand-1] + child[rand3])
        child = repair_chain(child)
    return child

def elitism(population):
    elites = []
    if probability(Pe):           
        elite_no = int(math.ceil(len(population)*Er))        
        population.sort(key = lambda s: len(s))
        elites = population[0:elite_no]    
    # print_population(elites)    
    return elites

# Input: Exponent exp
# Output: A valid addition chain = u1 , u2 , ..., ul = e with length l
def init_chain():    
    global exp
    chain = [1,2]    
    last = 1
    while(last < 4):
        chain.append(chain[last] + chain[random.randint(0,last)])
        last+=1     
    
    while (chain[last] != exp):        
        idx1 = last
        idx2 = -1
        # if probability(ds_rate):
        if probability(ds_rate) and last < exp//2:
            # Applying Double Stepping
            idx2 = last  
        else:
            option = random.randint(1,3)                        
            if (option == 1):
                # (a) Sum two preceding elements of the chain.
                idx2 = last-1                
            elif (option == 2):
                # (b) Sum the previous element and a random element.
                idx2 = random.randint(0, last)                                          
            else:
                # (d) Loop from the element on the position i − 1 until the largest element
                # that can be summed up with the last element is found.
                idx2 = last
                while(chain[idx1] + chain[idx2] > exp):
                    idx2 -= 1                
        chain.append(chain[idx1] + chain[idx2])        

        if chain[last+1] > exp:
            chain.pop(last+1)
            for i in range(last,-1,-1):
                if(chain[last] + chain[i] <= exp):
                    chain.append(chain[last] + chain[i])
                    break   

        last += 1        
    
    # print chain    
    # print is_valid_chain(chain)
    return chain

def get_args(sysargs):
    try:	
        global exp, pop_size, max_gen, Pc, Pm, Er, ds_rate, select_rate, n_mutants
        opts, args = getopt.getopt(sysargs, "", ("exp=", "pop_size=", "max_gen=", "pc=", "pm=", "er=", "ds_rate=", "select_rate=", "n_mutants="))		
        for opt,arg in opts:
            if opt == "--exp":
                exp = int(arg)
            elif opt == '--pop_size':
                pop_size = int(arg)
            elif opt == '--max_gen':
                max_gen = int(arg)
            elif opt == '--pc':
                Pc = float(arg)
            elif opt == '--pm':
                Pm = float(arg)
            elif opt == '--er':
                Er = float(arg)
            elif opt == '--ds_rate':
                ds_rate = float(arg)			
            elif opt == '--select_rate':
                select_rate = float(arg)			
            elif opt == '--n_mutants':
                n_mutants = int(arg)	
    except:
        print('Arguments parser error, try -h' + arg)		
        sys.exit(2)



# Require: POPSIZE is odd? or even?
# Input: An exponent exp
# Output: A quasi optimal addition chain U = u 1 , u 2 , ..., u l = e
def ga_addition_chain():
    global pop_size, max_gen, exp
    # Setting up initial population    
    population = []    
    for i in range(0, pop_size):
        population.append(init_chain())    
    # print population[i]
    
    
    for i in range(1, max_gen):        
        print "Generation ", i+1                
        next_generation = []
        for pop_idx in range(0, pop_size):                   
            # next_generation.append(population[pop_idx])
            child1, child2 = crossover(population[pop_idx], population[random.randint(0, pop_idx)])
            # Generate two new children by applying crossover operator*/}            
            # crossover(population[pop_idx], population[pop_idx+1], child1, child2)
            # Apply the mutation operator to each new created child
            # next_generation.extend(mutation(population[pop_idx]))
                
        # elitism        
        # next_generation.extend(elitism(population))        
        # next_generation.sort(key = lambda s: len(s))
        
        # Replace parents population with children fittest population            
        population = next_generation[0:pop_size]        
        # print_population(population)
    
    # Report fittest individual
    print "fittest"
    fittest = fittest_chain(population)
    print "L:", len(fittest), "  ", fittest
    print is_valid_chain(fittest)
    

# main
start = time.time()
get_args(sys.argv[1:])
ga_addition_chain()
end = time.time()
print(end - start)

 