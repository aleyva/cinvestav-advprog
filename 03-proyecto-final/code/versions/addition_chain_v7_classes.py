# -*- coding: utf-8 -*-
import sys
import getopt
import random
import operator
import time

# Global parameters
exp = 1000
pop_size = 10            # population Size
max_gen = 4             # Maximum number of generations
Pc = .004               # Crossover Rate (Pc)
Pm = .01                # Mutation Rate (Pm)
Pe = 0
ds_rate = .7            # Double Stepping Rate
select_rate = .2        # Probability of selecting   //select_last2
n_mutants = 4           # utilized in mutation operator = 4


class Chromosome:
    
    '''
    def __init__(self):
        self.chain = []
        self.length = 0
        self.init_chain()
        self.repair_and_fill()
    '''
    def __init__(self, chain_a=None):
        if chain_a is None:
            self.chain = []
            self.length = 0
            self.init_chain()        
        else:        
            self.chain = chain_a
            self.length = len(chain_a)        
        self.repair_and_fill()

    def __str__(self):
        return 'L:{} C:{}'.format(self.length, self.chain)        

    def init_chain(self):    
        global exp        
        self.chain = [1,2]    
        self.chain.append(2 + random.randint(1,2))
        self.length = 3  
        self.repair_and_fill()

    def append_item(self, item):
        self.chain.append(item)
        self.length += 1

    def append_item_idxs(self, idx1, idx2):        
        self.append_item(self.chain[idx1] + self.chain[idx2])        

    def is_valid_chain(self):
        for i in range(self.length-1,1,-1):
            difference = self.chain[i]-self.chain[i-1]
            if (type(self.chain.index(difference)) != int):
                return False
        return True

    def repair_and_fill(self):
        global exp
        chain = self.chain
        # last = len(chain)-1
        last = self.length-1
        # drop invalid elements
        while (chain[last] > exp):
            chain.pop(last)
            last-=1    
        
        # chain[last] defines the last element of the addition chain
        while (chain[last] != exp):
            rand = last
            if flip(ds_rate):
                # Applying Double Stepping(DS)
                chain.append(2*chain[last])
            elif flip(select_rate):
                # Selecting the last two elements
                chain.append(chain[last] + chain[last-1])
            else:
                # Selecting a random element
                rand = random.randint(0, last) 
                chain.append(chain[last] + chain[rand])            
            
            if chain[last+1] > exp:
                chain.pop(last+1)
                for i in range(rand-1,-1,-1):
                    if(chain[last] + chain[i] <= exp):
                        chain.append(chain[last] + chain[i])
                        break            
            last += 1        
            # print chain
        self.length = len(chain)

def flip(rate):
    rand = random.random()    
    if(rand > rate):
        return True
    else:
        return False

def print_population(population):
    length = len(population)
    for i in range(0,length):
        print population[i]

def fittest_chain(population):
    fittest = population[0]
    fittest_length = len(population[0])
    length = len(population)
    for i in range(1,length):
        if(len(population[i]) < fittest_length):
            fittest = population[i]
            fittest_length = len(population[i])
    return fittest

def gestate_crossover(parent1, parent2, crossover_point1, crossover_point2, length):
    # Generate child elements by using rules from parent1    
    child = parent1[0:crossover_point1+1]            
    # Generate child elements by using rules from parent2    
    for last in range(crossover_point1, crossover_point2):
        # Look for positions that define the rule for actual element,
        # such that parent2[i] = parent2a + parent2b
        idx = parent2.index(parent2[last+1]-parent2[last])                
        child.append(child[last] + child[idx])        
    
    for last in range(crossover_point2, length-1):
        # Look for positions that define the rule for actual element,
        # such that parent1[i] = parent1a + parent1b
        idx = parent1.index(parent1[last+1]-parent1[last])                
        child.append(child[last] + child[idx])        
       
    return child    

# Input: Parents P1 and P2 to be reproduced
# Output: Two new children, C1 and C2
def crossover(parent1, parent2, exp):
    if flip(Pc):
        # Set a common length length for both Parents (P1, P2)
        length = len(parent1)
        length_p2 = len(parent2)
        if(length_p2 < length):
            length = length_p2
        
        mid_length = length//2
        # Choose first crossover point
        crossover_point1 = random.randint(2, mid_length)
        # Choose second crossover point
        crossover_point2 = random.randint(mid_length +1, length - 2)        
        
        child1 = gestate_crossover(parent1, parent2, crossover_point1, crossover_point2, length)        
        child2 = gestate_crossover(parent2, parent1, crossover_point1, crossover_point2, length)

        #repair and fill childs
        child1 = fill(child1, len(child1)-1, exp)
        child2 = fill(child2, len(child2)-1, exp)
        
    else:
        # child1 will become a copy of parent1
        child1 = parent1[:]        
        # child2 will become a copy of parent2
        child2 = parent2[:]   
    return child1, child2

# Input: A Child Individual child = (U, l).
# Output: The Best Mutated Child Version Cm = (U m , l)
def mutation(child):
    global exp
    if flip(Pm):               
        mutants = []
        # Select a common limit to generate mutation point
        limit_idx = random.randint(2, child.length-1)
        # Generate N mutants of the Child Individual        
        for mutant_idx in range(0,n_mutants):
            rand = random.randint(0, limit_idx-1)          
            mutant = Chromosome(child[0:limit_idx+1])
            mutant.append_item_idxs(limit_idx,rand)
            mutant.repair_and_fill()
            mutants.append(mutant)
        
        # Choose the best mutation version.        
        # el hijo puede ser mas fittest que los padres, ¿si es mejor, se debe comparar para dejarlo?
        return fittest_chain(mutants)

def get_args(sysargs):
    try:	
        global exp, pop_size, max_gen, Pc, Pm, Er, ds_rate, select_rate, n_mutants
        opts, args = getopt.getopt(sysargs, "", ("exp=", "pop_size=", "max_gen=", "pc=", "pm=", "er=", "ds_rate=", "select_rate=", "n_mutants="))		
        for opt,arg in opts:
            if opt == "--exp":
                exp = int(arg)
            elif opt == '--pop_size':
                pop_size = int(arg)
            elif opt == '--max_gen':
                max_gen = int(arg)
            elif opt == '--pc':
                Pc = float(arg)
            elif opt == '--pm':
                Pm = float(arg)
            elif opt == '--er':
                Er = float(arg)
            elif opt == '--ds_rate':
                ds_rate = float(arg)			
            elif opt == '--select_rate':
                select_rate = float(arg)			
            elif opt == '--n_mutants':
                n_mutants = int(arg)	
    except:
        print('Arguments parser error, try -h' + arg)		
        sys.exit(2)
		

# Require: POPSIZE is odd? or even?
# Input: An exponent exp
# Output: A quasi optimal addition chain U = u 1 , u 2 , ..., u l = e
def ga_addition_chain():
    global pop_size, max_gen, exp
    # Setting up initial population    
    population = []
    for i in range(0, pop_size):
        population.append(Chromosome())    
    print print_population(population)
    
    # print "fittest_init"
    # print "\t", fittest_chain(population)

    for i in range(0, max_gen):
        # Randomize parents population or shuffle population?        
        random.shuffle(population)
        next_generation = []
        for pop_idx in range(0, pop_size, 2):                        
            child1 = population[pop_idx]
            child2 = population[pop_idx+1]
            # child1, child2 = crossover(population[pop_idx], population[pop_idx+1], exp)

            # Generate two new children by applying crossover operator*/}            
            # crossover(population[pop_idx], population[pop_idx+1], child1, child2)
            # Apply the mutation operator to each new created child
            mutation(child1, exp)
            mutation(child2, exp)
            next_generation.append(child1)
            next_generation.append(child2)
        
        # Replace parents population with children population    
        population = next_generation
        # print_population(population)
        # print "fittest in gen ", i+1
        # print "\t", fittest_chain(population)
    # Report fittest individual
    print "fittest"
    print fittest_chain(population)


# main
start = time.time()
get_args(sys.argv[1:])
ga_addition_chain()
end = time.time()
print(end - start)