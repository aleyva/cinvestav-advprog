
# -*- coding: utf-8 -*-
import math
import sys

def print_chain(chain):
    str_chain = [str(item) for item in chain]
    print(", " . join(str_chain))

def build_chain(goal):
    chain = [1]    
    chain_idx = 0
    goal_bin = bin(goal).replace("0b","") 
    goal_str = map(int,str(goal_bin))
    #print (goal_str)
    for i in range( 1,len(goal_str)):        
        chain.append( chain[chain_idx]*2 )
        chain_idx += 1
        if(goal_str[i] == 1):
            chain.append( chain[chain_idx]+1 )
            chain_idx += 1
    return chain

# __Main__
n = len(sys.argv)
if n != 2 :
	print "ERROR: Requerido 1 argumento; numero para formar cadena de adición"
	sys.exit(1)

goal = int(sys.argv[1])
if goal <= 1 :
	print "ERROR: El argumento debe ser un natural mayor a 2"
	sys.exit(1)    

chain = build_chain(goal)
print_chain(chain)