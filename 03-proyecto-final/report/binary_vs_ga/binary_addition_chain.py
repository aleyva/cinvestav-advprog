
# -*- coding: utf-8 -*-
import math
import sys

class binary_addition_chain:	

        def __init__(self):
	        pass

        def build_chain(self, goal):
                chain = [1]    
                chain_idx = 0
                goal_bin = bin(goal).replace("0b","") 
                goal_str = map(int,str(goal_bin))
                #print (goal_str)
                for i in range( 1,len(goal_str)):        
                        chain.append( chain[chain_idx]*2 )
                        chain_idx += 1
                        if(goal_str[i] == 1):
                                chain.append( chain[chain_idx]+1 )
                                chain_idx += 1
                return chain

