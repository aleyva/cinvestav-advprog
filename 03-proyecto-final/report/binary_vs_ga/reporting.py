# -*- coding: utf-8 -*-
import sys
import getopt
import binary_addition_chain from binary_addition_chain
import ga_addition_chain from ga_addition_chain 


def get_args(sysargs):
    try:	
        global exp, pop_size, max_gen, Pc, Pm, Er, ds_rate, select_rate, n_mutants
        opts, args = getopt.getopt(sysargs, "", ("exp=", "pop_size=", "max_gen=", "pc=", "pm=", "er=", "ds_rate=", "select_rate=", "n_mutants=", "rep_gen="))		
        for opt,arg in opts:
            if opt == "--exp":
                exp = int(arg)
            elif opt == '--pop_size':
                pop_size = int(arg)
            elif opt == '--max_gen':
                max_gen = int(arg)
            elif opt == '--pc':
                Pc = float(arg)
            elif opt == '--pm':
                Pm = float(arg)
            elif opt == '--er':
                Er = float(arg)
            elif opt == '--ds_rate':
                ds_rate = float(arg)			
            elif opt == '--select_rate':
                select_rate = float(arg)			
            elif opt == '--n_mutants':
                n_mutants = int(arg)
            elif opt == '--rep_gen':
                rep_gen = int(arg)
                	
    except:
        print('Arguments parser error, try -h' + arg)		
        sys.exit(2)


# __Main__
n = len(sys.argv)
if n < 2 :
	print "ERROR: Requerido 1 argumento; numero para formar cadena de adición"
	sys.exit(1)


# # ga_addition_chain
# chain = build_chain(goal)
# print_chain(chain)

# # ga_addition_chain
# start = time.time()
# get_args(sys.argv[1:])
# ga_addition_chain()
# end = time.time()
# print "\nTotal time: ", (end - start)

 
# # Report fittest
#         print "\n--- FITTEST ---"
#         fittest = fittest_chain(population)
#         print "Length:", len(fittest), "\tIs valid chain? ", is_valid_chain(fittest)
#         print fittest    